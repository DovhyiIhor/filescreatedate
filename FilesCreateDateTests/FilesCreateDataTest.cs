using FilesCreateDate;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FilesCreateDateTests
{
    [TestClass]
    public class FilesCreateDataTest
    {
        [TestMethod]
        public void FilesWorkerTest_MethodExecute_CorrectDataExpected()
        {
            // Arrange
            var testFilesWorker = new FilesWorker(pathToFile: "TestFolder");
            var expected = new List<AboutFile>() { new AboutFile() { Name = @"TestFolder\TestFolder1", CreateDate = new DateTime(2021, 6, 1, 18, 2, 26) },
                                                   new AboutFile() { Name = @"TestFolder\TestFolder2", CreateDate = new DateTime(2021, 6, 1, 18, 3, 5) },
                                                   new AboutFile() { Name = @"TestFolder\TestTXT1.txt", CreateDate = new DateTime(2021, 6, 1, 18, 3, 24) },
                                                   new AboutFile() { Name = @"TestFolder\TestTXT_2.txt", CreateDate = new DateTime(2021, 6, 1, 18, 4, 2)} };

            // Act
            var actual = testFilesWorker.Execute();

            // Assert
            Assert.AreEqual(expected: expected.Count,actual: actual.Count);
            Assert.AreEqual(expected: expected[0].Name,actual: actual[0].Name);
        }

        [TestMethod]
        public void FilesWorkerTest_MethodExist_CorrectExistIsTrueExpected()
        {
            // Arrange
            var testFilesWorker = new FilesWorker(pathToFile: "TestFolder");

            // Act
            var actual = testFilesWorker.IsExist();

            // Assert
            Assert.IsTrue(condition: actual);
        }

        [TestMethod]
        public void ResultWriterTest_MethodExecuteByDate_CorrectWriteInFileExpected()
        {
            // Arrange
            var testFolder1 = new AboutFile() { Name = @"TestFolder\TestFolder1", CreateDate = new DateTime(2021, 6, 1, 18, 2, 26) };
            var testFolder2 = new AboutFile() { Name = @"TestFolder\TestFolder2", CreateDate = new DateTime(2021, 6, 1, 18, 3, 5) };
            var testTxt1 = new AboutFile() { Name = @"TestFolder\TestTXT_1.txt", CreateDate = new DateTime(2021, 6, 1, 18, 3, 24) };
            var testTxt2 = new AboutFile() { Name = @"TestFolder\TestTXT_2.txt", CreateDate = new DateTime(2021, 6, 1, 18, 4, 2) } ;

            var testList = new List<AboutFile>() { new AboutFile() { Name = @"TestFolder\TestFolder1", CreateDate = new DateTime(2021, 6, 1, 18, 2, 26) },
                                                   new AboutFile() { Name = @"TestFolder\TestFolder2", CreateDate = new DateTime(2021, 6, 1, 18, 3, 5) },
                                                   new AboutFile() { Name = @"TestFolder\TestTXT_1.txt", CreateDate = new DateTime(2021, 6, 1, 18, 3, 24) },
                                                   new AboutFile() { Name = @"TestFolder\TestTXT_2.txt", CreateDate = new DateTime(2021, 6, 1, 18, 4, 2)} };

            var testResultWriter = new ResultWriter(aboutFiles: testList, path: "TestFolder");

            var expected = new List<string>() { testFolder1.ToString(), testFolder2.ToString(), testTxt1.ToString(), testTxt2.ToString() };

            // Act
            testResultWriter.ExecuteByDate();
            var actual = ReadFromFile();

            // Assert
            CollectionAssert.AreEqual(expected: expected, actual: actual);
        }
        
        public List<string> ReadFromFile()
        {
            var toReturn = new List<string>();

            var path = "TestFolder.txt";

            using (StreamReader stream = new StreamReader(path: path, System.Text.Encoding.Default))
            {
                string line;
                while ((line = stream.ReadLine()) != null)
                {
                    toReturn.Add(line);
                }
            }

            return toReturn;
        }
    }
}
