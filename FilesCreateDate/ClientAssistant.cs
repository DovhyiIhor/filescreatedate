﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilesCreateDate
{
    public class ClientAssistant
    {
        private string _pathToFile;
        private string _pathToSave;
        ResultWriter _resultWriter;
        FilesWorker _filesWorker;

        private void GetPathToWork()
        {
            Console.Write("Give me a path to my work: ");
            _pathToFile = CorrectInput();
            _filesWorker = new FilesWorker(pathToFile: _pathToFile);
        }

        private bool IsWorkFileExist()
        {
            return _filesWorker.IsExist();
        }

        private bool IsSaveFileExist()
        {
            var checkSaveFile = new FilesWorker(pathToFile: _pathToSave);
            return checkSaveFile.IsExist();
        }

        private string CorrectInput()
        {
            var key = true;
            var inputData = "";
            while (key)
            {
                inputData = Console.ReadLine();
                if (!String.IsNullOrEmpty(value: inputData))
                {
                    key = false;
                }
                else
                {
                    Console.WriteLine("Input is incorrect");
                }
            }

            return inputData;
        }

        private void SaveResultToFile()
        {
            Console.Write("Where can I save it? ");

            _pathToSave = CorrectInput();

            _resultWriter = new ResultWriter(aboutFiles: _filesWorker.Execute(), path: _pathToSave);
            _resultWriter.ExecuteByDate();
        }

        public void Run()
        {
            var key = true;
            while (key)
            {
                GetPathToWork();

                if (IsWorkFileExist())
                {
                    SaveResultToFile();

                    if(IsSaveFileExist())
                    {
                        key = false;
                        Console.Write("Done");
                    }
                    else
                    {
                        ErrorMessage();
                    }
                }
                else
                {
                    ErrorMessage();
                }
            }
        }

        private void ErrorMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Something went wrong");
            Console.ResetColor();
        }
    }
}
