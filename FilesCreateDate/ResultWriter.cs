﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;


namespace FilesCreateDate
{
    public class ResultWriter
    {
        private string _path;
        private List<AboutFile> _writeToTxt;

        public ResultWriter(List<AboutFile> aboutFiles, string path)
        {
            _writeToTxt = aboutFiles;
            _path = path;
        }

        public void ExecuteByDate()
        {
            using (StreamWriter stream = new StreamWriter(path: _path +  ".txt",append: false,encoding: Encoding.Default))
            {
                foreach(var file in _writeToTxt.OrderBy(keySelector: x => x.CreateDate))
                {
                    stream.Write(value: file + "\n");
                }
            }
        }
    }
}
