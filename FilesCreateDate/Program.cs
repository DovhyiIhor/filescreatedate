﻿using System;

namespace FilesCreateDate
{
    class Program
    {
        static void Main(string[] args)
        {
            var clientAssistant = new ClientAssistant();
            clientAssistant.Run();
        }
    }
}
