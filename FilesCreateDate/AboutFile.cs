﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilesCreateDate
{
    public class AboutFile
    {
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }

        public override string ToString()
        {
            return this.Name + " " + CreateDate.ToString();
        }
    }
}
