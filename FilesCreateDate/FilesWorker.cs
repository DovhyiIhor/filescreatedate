﻿using System.Collections.Generic;
using System.IO;

namespace FilesCreateDate
{
    public class FilesWorker
    {
        private string _path;
        private DirectoryInfo _directoryInfo;
        private List<AboutFile> _aboutFile;

        public FilesWorker(string pathToFile)
        {
            _path = pathToFile;
            _directoryInfo = new DirectoryInfo(path: _path);
            _aboutFile = new List<AboutFile>();
        }

        public bool IsExist()
        {
            return _directoryInfo.Exists; ;
        }

        private void SubDirectorys()
        {
           var directorys = Directory.GetDirectories(path: _path);

            foreach(var directory in directorys)
            {
                _aboutFile.Add(new AboutFile() { Name = directory, CreateDate = new FileInfo(directory).CreationTime });
            }
        }

        private void SubFiles()
        {
            var files = Directory.GetFiles(path: _path);

            foreach (var file in files)
            {
                _aboutFile.Add(new AboutFile() { Name = file, CreateDate = new FileInfo(fileName: file).CreationTime });
            }
        }

        public List<AboutFile> Execute()
        {
            SubDirectorys();
            SubFiles();
            return _aboutFile;
        }
    }
}
